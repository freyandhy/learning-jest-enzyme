// import { render, screen } from '@testing-library/react';
import App from './App';

import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({ adapter: new Adapter() });

describe("Counter Testing", () => {
  test("render title apps", () => {
    const wrapper = shallow(<App />)
    expect(wrapper.find("h1").text()).toContain("This is Counter App");
  })

  test("render a button with text of `Plus`", () => {
    const wrapper = shallow(<App />)
    expect(wrapper.find("#btn-plus").text()).toBe("Plus")
  })

  test("render a button with text of `Minus`", () => {
    const wrapper = shallow(<App />)
    expect(wrapper.find("#btn-minus").text()).toBe("Minus")
  })

  test("render initial value", () => {
    const wrapper = shallow(<App />)
    expect(wrapper.find(".value").text()).toBe("0")
  })

  test("Simulate `Plus` button", () => {
    const wrapper = shallow(<App />)
    wrapper.find("#btn-plus").simulate("click")
    expect(wrapper.find(".value").text()).toBe("1")
  })

  test("Simulate `Minus` button", () => {
    const wrapper = shallow(<App />)
    wrapper.find("#btn-plus").simulate("click")
    expect(wrapper.find(".value").text()).toBe("1")
    wrapper.find("#btn-plus").simulate("click")
    expect(wrapper.find(".value").text()).toBe("2")
    wrapper.find("#btn-minus").simulate("click")
    expect(wrapper.find(".value").text()).toBe("1")
    wrapper.find("#btn-minus").simulate("click")
    expect(wrapper.find(".value").text()).toBe("0")
  })
})

// test('renders learn react link', () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });
