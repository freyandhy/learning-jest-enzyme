import React, { useState } from "react"
import './App.css';

function App() {
  const [value, setValue] = useState(0)
  const handlePlus = () => setValue(value + 1)
  const handleMinus = () => setValue(value - 1)
  
  return (
    <div className="App">
      <h1>This is Counter App</h1>
      <div className="value">{value}</div>
      <button id="btn-plus" onClick={handlePlus}>Plus</button>
      <button id="btn-minus" onClick={handleMinus}>Minus</button>
    </div>
  );
}

export default App;
